<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::group(array('before'=> 'auth'),
	function()
	{
		Route::any("/", array(
				"as"  => "auth/login",
				"uses"=> "AuthController@login"
			));

		Route::any("/register", array(
				"as"  => "auth/register",
				"uses"=> "AuthController@register"
			));

	});





Route::any("/logout", array(
		"as"  => "auth/logout",
		"uses"=> "AuthController@logout"
	));

//admin Routes
Route::group(array('before'=> 'checkpermission:2'),
	function()
	{
		Route::any("/admin", array(
				"as"  => "admin/dashboard",
				"uses"=> "AdminController@dashboard"
			));

		Route::any("/admin/applications", array(
				"as"  => "admin/applications",
				"uses"=> "AdminController@userleaves"
			));
		Route::any("/admin/history", array(
				"as"  => "admin/history",
				"uses"=> "AdminController@userleavehistory"
			));

	});
//User Routes
Route::group(array('before'=> 'checkpermission:1'),
	function()
	{
		Route::any("/user", array(
				"as"  => "user/dashboard",
				"uses"=> "UserController@dashboard"
			));

		Route::any("/user/applyleave", array(
				"as"  => "user/applyforleave",
				"uses"=> "UserController@leave"
			));
		Route::any("/user/leavehistory", array(
				"as"  => "user/leavehistory",
				"uses"=> "UserController@history"
			));
	});