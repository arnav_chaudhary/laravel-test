@extends("layout")
@section("content")
<table>
<tr>
<td>
	From Date
</td>
<td>
	To Date
</td>
<td>
	Reason
</td>
<td>
	By
</td>
<td>
	Accept
</td>
<td>
	Reject
</td>
</tr>
@foreach($data as $pending)
<tr>
	<td>
		{{$pending->from_date}}
	</td>
	<td>
		{{$pending->to_date}}
	</td>
	<td>
		{{$pending->reason}}
	</td>
	<td>
		{{$pending->username}}
	</td>
	<td>
	{{ Form::open() }}
    {{ Form::hidden("id",$pending->id) }}
    {{ Form::hidden("type","approve") }}
    {{ Form::submit("Approve") }}
  	{{ Form::close() }}
	</td>
	<td>
	{{ Form::open() }}
    {{ Form::hidden("id",$pending->id) }}
    {{ Form::hidden("type","reject") }}
    {{ Form::submit("Reject") }}
  	{{ Form::close() }}
	</td>
	
</tr>
@endforeach
</table>	
@stop