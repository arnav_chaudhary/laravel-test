@extends("layout")
@section("content")
<table>
<tr>
<td>
	From Date
</td>
<td>
	To Date
</td>
<td>
	Reason
</td>
<td>
	Status
</td>
</tr>
@foreach($history as $data)
<tr>
	<td>
		{{$data->from_date}}
	</td>
	<td>
		{{$data->to_date}}
	</td>
	<td>
		{{$data->reason}}
	</td>
	<td>
		@if($data->status==0)
			Pending
		@elseif($data->status==1)
			Granted
		@elseif($data->status==2)
			Rejected
		@endif
	</td>
	
</tr>
@endforeach
</table>	
@stop