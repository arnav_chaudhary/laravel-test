<?php

class UserController
extends Controller
{
	public function dashboard()
	{
		return View::make('user/dashboard');
	}

	protected function getLeaveValidator()
	{
		$from_date = Input::get("from_date");
		return Validator::make(Input::all(), [
				"from_date" => "required|date|date_format:Y-m-d|after:".date("Y-m-d"),
				"to_date" => "required|date|date_format:Y-m-d|after:from_date|before:".date("Y-m-d",strtotime("$from_date +7day")),
				"reason" => "required"
			]);
	}
	protected function isPostRequest()
	{
		return Input::server("REQUEST_METHOD") == "POST";
	}

protected function getLeaveData()
	{
		return [
			"user_id" => Auth::user()->id,
			"from_date" => date('Y-m-d', strtotime(Input::get("from_date"))),
			"to_date"    => Input::get("to_date"),
			"status"    => 0,
			"reason"    => Input::get("reason")
		];
	}
	public function leave()
	{
		if($this->isPostRequest()){
			$validator = $this->getLeaveValidator();

			if($validator->passes()){
				$leave_data = $this->getLeaveData();
				Leave::create($leave_data);
				return Redirect::to("/user/leavehistory");
				
			}
			else
			{
				return Redirect::back()
				->withInput()
				->withErrors($validator);
			}
		}
		return View::make('user/applyleave');
	}

	public function history()
	{
		
		$history =  Leave::where('user_id', '=', Auth::user()->id)->get();
		
		return View::make('user/seehistory',array("history"=>$history);
	}

}