<?php

class AdminController
extends Controller
{
	public function dashboard()
	{
		return View::make('admin/dashboard');
	}

	public function userleaves()
	{
		if(Input::server("REQUEST_METHOD") == "POST"){
			if(Input::get("type") == "approve")
			{

				DB::table('leave')
				->where('id', Input::get("id"))
				->update(array('status'=> 1));
			}
			elseif(Input::get("type") == "reject")
			{
				DB::table('leave')
				->where('id', Input::get("id"))
				->update(array('status'=> 2));

			}
		}


		$pending = Leave::where('status', '=', 0)
		->join('user', 'user.id', '=', 'leave.user_id')
		->select('leave.id', 'user.username', 'leave.from_date','leave.to_date','leave.reason')
		->get();

		return View::make('admin/leaves',array("data"=>$pending));
	}

	public function userleavehistory()
	{
		$leave = Leave::join('user', 'user.id', '=', 'leave.user_id')
		->select('leave.id', 'user.username', 'leave.from_date','leave.to_date','leave.reason','leave.status')
		->get();
		return View::make('admin/history',array("history"=>$leave));

	}
}