<?php

class AuthController
extends Controller
{
	public function login()
	{
		if($this->isPostRequest())
		{
			$validator = $this->getLoginValidator();

			if($validator->passes())
			{
				$credentials = $this->getLoginCredentials();

				if(Auth::attempt($credentials,true))
				{
					return Redirect::to("/");
				}

				return Redirect::back()->withErrors([
						"password" => ["Credentials invalid."]
					]);
			}
			else
			{
				return Redirect::back()
				->withInput()
				->withErrors($validator);
			}
		}

		return View::make("auth/login");
	}

	protected function isPostRequest()
	{
		return Input::server("REQUEST_METHOD") == "POST";
	}

	protected function getLoginValidator()
	{
		return Validator::make(Input::all(), [
				"username" => "required",
				"password" => "required",
			]);
	}

	protected function getLoginCredentials()
	{
		return [
			"username" => Input::get("username"),
			"password" => Input::get("password")
		];
	}
	public function logout()
	{
		Auth::logout();
		return View::make("auth/logout");
	}
	
	protected function getRegisterValidator()
	{
		return Validator::make(Input::all(), [
				"username" => "required|unique:user,username",
				"password" => "required",
				"email"=> "required|unique:user,email|email"
			]);
	}
	
	protected function getRegistrationCredentials()
	{
		return [
			"username" => Input::get("username"),
			"password" => Hash::make(Input::get("password")),
			"email"    => Input::get("email"),
			"user_type_id" => 1
		];
	}
	
	public function register()
	{
		if($this->isPostRequest())
		{
			$validator = $this->getRegisterValidator();

			if($validator->passes())
			{
				$credentials = $this->getRegistrationCredentials();
				$newUser = User::create($credentials);
				$credentials = $this->getLoginCredentials();
				if(Auth::attempt($credentials,true))
				{
					return Redirect::to("/");
				}
			
			}
			else
			{
				return Redirect::back()
				->withInput()
				->withErrors($validator);
			}
		}

		return View::make("auth/register");
	}

}