<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration
{

	/**
	* Run the migrations.
	*
	* @return void
	*/
	public function up()
	{
		Schema::create("leave",
			function(Blueprint $table)
			{
				$table->increments("id");
				$table->integer("user_id")->unsigned();
				$table->foreign('user_id')->references('id')->on('user');
				$table->date('from_date');
				$table->date('to_date');
				$table->integer('status');
				$table->text('reason');	
				$table->timestamps();
			});
	}

	/**
	* Reverse the migrations.
	*
	* @return void
	*/
	public function down()
	{
		Schema::table('leave',
			function(Blueprint $table)
			{
				Schema::dropIfExists("leave");
			});
	}

}
