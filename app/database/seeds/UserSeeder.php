<?php
 
class UserSeeder
  extends DatabaseSeeder
{
  public function run()
  {
    $users = [
      [
        "username" => "admin",
        "password" => Hash::make("admin"),
        "email"    => "admin@admin.com",
        "user_type_id" => "2"
      ]
    ];
  
    foreach ($users as $user) {
      User::create($user);
    }
  }
}